SELECT kabupaten.nama_kabupaten, COUNT(*) AS jumlah_instansi
FROM instansi
JOIN kabupaten ON instansi.kabupaten_id = kabupaten.id
WHERE kabupaten.nama_kabupaten IN ('Bireuen', 'Bener Meriah')
GROUP BY kabupaten.nama_kabupaten;
