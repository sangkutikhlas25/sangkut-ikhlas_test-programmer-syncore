SELECT m.* 
FROM members m
JOIN kabupaten k ON m.kabupaten_id = k.id
JOIN provinsi p ON k.provinsi_id = p.id
WHERE p.nama_provinsi = 'Sumatera Utara' AND k.nama_kabupaten = 'KOTA MEDAN';
