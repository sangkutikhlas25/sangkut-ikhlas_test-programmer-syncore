SELECT kabupaten.*
FROM kabupaten
LEFT JOIN members ON kabupaten.id = members.kabupaten_id
WHERE members.kabupaten_id IS NULL;
