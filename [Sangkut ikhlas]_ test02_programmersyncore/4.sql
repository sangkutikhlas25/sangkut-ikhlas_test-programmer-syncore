SELECT kecamatan.*
FROM kecamatan
LEFT JOIN members ON kecamatan.id = members.kecamatan_id
WHERE members.kecamatan_id IS NULL;
